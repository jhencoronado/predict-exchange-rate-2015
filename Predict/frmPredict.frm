VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmPredict 
   Caption         =   "Predicted Exhange Rate 2015"
   ClientHeight    =   1620
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   4710
   OleObjectBlob   =   "frmPredict.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "frmPredict"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub cmdPredict_Click()
    Dim x As Range
    Dim y As Range
    Dim ans1, ans2, ans3, ans4, ans5, ans6, ans7, ans8, ans9, ans10, ans11, ans12 As Double
    Set x = Sheet1.Range("B16:J16")
    Set y = Sheet1.Range("B4:J4")
    ans1 = Format(Application.WorksheetFunction.Forecast(40, y, x), "##,##0.00")
    Set x = Sheet1.Range("B15:J15")
    Set y = Sheet1.Range("B4:J4")
    ans2 = Format(Application.WorksheetFunction.Forecast(40, y, x), "##,##0.00")
    Set x = Sheet1.Range("B14:J14")
    Set y = Sheet1.Range("B4:J4")
    ans3 = Format(Application.WorksheetFunction.Forecast(40, y, x), "##,##0.00")
    Set x = Sheet1.Range("B13:J13")
    Set y = Sheet1.Range("B4:J4")
    ans4 = Format(Application.WorksheetFunction.Forecast(40, y, x), "##,##0.00")
    Set x = Sheet1.Range("B12:J12")
    Set y = Sheet1.Range("B4:J4")
    ans5 = Format(Application.WorksheetFunction.Forecast(40, y, x), "##,##0.00")
    Set x = Sheet1.Range("B11:J11")
    Set y = Sheet1.Range("B4:J4")
    ans6 = Format(Application.WorksheetFunction.Forecast(40, y, x), "##,##0.00")
    Set x = Sheet1.Range("B10:J10")
    Set y = Sheet1.Range("B4:J4")
    ans7 = Format(Application.WorksheetFunction.Forecast(40, y, x), "##,##0.00")
    Set x = Sheet1.Range("B9:J9")
    Set y = Sheet1.Range("B4:J4")
    ans8 = Format(Application.WorksheetFunction.Forecast(40, y, x), "##,##0.00")
    Set x = Sheet1.Range("B8:J8")
    Set y = Sheet1.Range("B4:J4")
    ans9 = Format(Application.WorksheetFunction.Forecast(40, y, x), "##,##0.00")
    Set x = Sheet1.Range("B7:J7")
    Set y = Sheet1.Range("B4:J4")
    ans10 = Format(Application.WorksheetFunction.Forecast(40, y, x), "##,##0.00")
    Set x = Sheet1.Range("B6:J6")
    Set y = Sheet1.Range("B4:J4")
    ans11 = Format(Application.WorksheetFunction.Forecast(40, y, x), "##,##0.00")
    Set x = Sheet1.Range("B5:J5")
    Set y = Sheet1.Range("B4:J4")
    ans12 = Format(Application.WorksheetFunction.Forecast(40, y, x), "##,##0.00")
    
    MsgBox "January: " & ans1 & vbNewLine & "February: " & ans2 & vbNewLine & "March: " & ans3 & vbNewLine & "April: " & ans4 & vbNewLine & "May: " & ans5 & vbNewLine & "June: " & ans6 & vbNewLine & "July: " & ans7 & vbNewLine & "August: " & ans8 & vbNewLine & "September: " & ans9 & vbNewLine & "October: " & ans10 & vbNewLine & "November: " & ans11 & vbNewLine & "December: " & ans12, vbInformation + vbOKOnly, "Predicted Exchange Rate 2015"
End Sub
